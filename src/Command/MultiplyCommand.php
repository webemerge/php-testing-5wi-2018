<?php

namespace App\Command;

use App\Calculator\SimpleCalculator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MultiplyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('calc:mul')
            ->addArgument('var1', InputArgument::REQUIRED)
            ->addArgument('var2', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $calculator = new SimpleCalculator();

        $sum1 = $input->getArgument('var1');
        $sum2 = $input->getArgument('var2');
        $result = $calculator->multiply($sum1, $sum2);

        $output->writeln("$sum1 x $sum2 = $result");
    }
}
