<?php

namespace App\Command;

use App\Calculator\SimpleCalculator;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DivideCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('calc:div')
            ->addArgument('dividend', InputArgument::REQUIRED)
            ->addArgument('divisor', InputArgument::REQUIRED)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $calculator = new SimpleCalculator();

        $dividend = $input->getArgument('dividend');
        $divisor = $input->getArgument('divisor');
        $result = $calculator->divideBy($dividend, $divisor);

        $output->writeln("$dividend / $divisor = $result");
    }
}
