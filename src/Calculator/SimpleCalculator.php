<?php

namespace App\Calculator;

class SimpleCalculator
{
    public function add($var1, $var2)
    {
        return $var1 + $var2;
    }

    public function divideBy($dividend, $divisor)
    {
        if (0 == $divisor) {
            throw new DivisionByZeroException('Division by zero!');
        }

        return $dividend / $divisor;
    }

    public function multiply($var1, $var2)
    {
        return $var1 * $var2;
    }
}
