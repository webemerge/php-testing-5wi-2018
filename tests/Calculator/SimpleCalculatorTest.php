<?php

namespace App\Tests\Calculator;

use App\Calculator\DivisionByZeroException;
use App\Calculator\SimpleCalculator;
use PHPUnit\Framework\TestCase;

class SimpleCalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calculator = new SimpleCalculator();
        $result = $calculator->add(12, 15);

        $this->assertEquals(27, $result);
    }

    public function testAddRandomPositive()
    {
        $calculator = new SimpleCalculator();
        $var1 = mt_rand(1, pow(2, 15));
        $var2 = mt_rand(1, pow(2, 15));
        $result = $calculator->add($var1, $var2);

        $this->assertGreaterThan(0, $result);
    }

    public function testDivideBy()
    {
        $calculator = new SimpleCalculator();
        $result = $calculator->divideBy(12, 4);

        $this->assertEquals(3, $result);
    }

    public function testDivisionByZeroThrowsException()
    {
        $calculator = new SimpleCalculator();
        $this->expectException(DivisionByZeroException::class);
        $result = $calculator->divideBy(12, 0);
    }

    public function testMultiplying2NegativeNumbersResultsInPositiveNumber()
    {
        $calculator = new SimpleCalculator();
        $result = $calculator->multiply(-2, -3);

        $this->assertSame(6, $result);
    }
}
